package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;

public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                }
                else if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
                else {
                    currentGeneration.set(row, col, CellState.DYING);
                }
            }
        }
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        for (int i=0; i<numberOfRows(); i++) {
            for (int j = 0; j < numberOfColumns(); j++) {
                nextGeneration.set(i, j, getNextCell(i, j));
            }
        }
        currentGeneration = nextGeneration;
    }

    @Override
    public CellState getNextCell(int row, int col) {
        CellState current = currentGeneration.get(row, col);
        CellState cellState = getCellState(row, col);
        int myNeighbours = countNeighbors(row, col, CellState.ALIVE);
        if (cellState == CellState.ALIVE) {
            return CellState.DYING;
        }
        else if (cellState == CellState.DYING) {
            return CellState.DEAD;
        }
        else if ((cellState == CellState.DEAD)) {
            if (myNeighbours == 2) {
                return CellState.ALIVE;
            }
            else {
                return CellState.DEAD;
            }
        }
        return current;

    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }

    private int countNeighbors(int row, int col, CellState state) {
        int aliveCount = 0;
        try {
            for (int i = row-1; i<=row+2; i++) {
                for (int j = col-1; j<= col+2; j++) {
                    if (i<0 && j<0) {
                        continue;
                    }
                    if (i>=numberOfRows() || j>=numberOfColumns()) {
                        continue;
                    }
                    if (i == row && j == col) {
                        continue;
                    }
                    else if (getCellState(i, j).equals(state)) {
                        aliveCount++;
                    }
                }
            }
        }
        catch (Exception e) {
        }
        return aliveCount;
    }
}
