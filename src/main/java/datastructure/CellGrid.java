package datastructure;

import cellular.CellState;


public class CellGrid implements IGrid {

    private int rows;
    private int cols;
    private CellState initialState;
    private CellState[][] cellGrid;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.cols = columns;
        this.initialState = initialState;
        cellGrid = new CellState[rows][columns];
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < columns; col++) {
                this.cellGrid[row][col] = initialState;
            }
        }
		// TODO Auto-generated constructor stub
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row >= 0 && row < numRows() && column >= 0 && column < numColumns()) {
            cellGrid[row][column] = element;
        }
        else {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public CellState get(int row, int column) {
        if (row >= 0 && row < numRows() && column >= 0 && column < numColumns()) {
            return cellGrid[row][column];
        }
        else {
            throw new IndexOutOfBoundsException();
        }
        // TODO Auto-generated method stub
    }

    @Override
    public IGrid copy() {
        CellGrid copyGrid = new CellGrid(numRows(), numColumns(), CellState.DEAD);
        for (int i=0; i<cellGrid.length; i++) {
            for (int j=0; j<cellGrid[i].length; j++) {
                copyGrid.set(i, j, this.get(i, j));
            }
        }
        return copyGrid;
        // TODO Auto-generated method stub
    }
}
